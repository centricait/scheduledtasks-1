#* FileName: Build-ScheduledTaskDefault.ps1
#*==========================================================================================
#* Script Name: build
#* Created: 09/02/2012
#* Author: Lloyd Holman
#* Company: Infigo Software Ltd

#* Requirements:
#* 1. Install PowerShell 2.0+ on local machine
#* 2. Execute from build.bat

#*==========================================================================================
#* Purpose: Performs the grunt of the psake based execution of scheduled tasks 
#*==========================================================================================
#*==========================================================================================
#* SCRIPT BODY
#*==========================================================================================
Properties { 
	$baseDir = Resolve-Path .
	$7ZipPath = "lib\7-Zip\7z.exe"
	$psFtpPath = "lib\psftp.exe"
	$encryptionKey = $p1
	$sftpUser = $p2
	$sftpPassword = $p3
	$reportsFolderName = $p4
	$script:archiveFileName = $p5
	$ftpUri = $p6
	$source = "http://www.musicsales.com/feeds/custombooks.xml"
	$versionSource = "http://www.musicsales.com/feeds/version.xml"
	$versionDestination = [String]::Format("{0}\Musicsales\productFeedVersion.xml",$baseDir)
	$web = New-Object System.Net.WebClient
	$elasticsearchUrl = "http://172.30.107.107:9200" 
	$archiveFile = ""
	$reportsPath = ""
}

$ErrorActionPreference = 'Stop'

Task default -depends Help

Task Help {
	Write-Host "List ScheduledTasks here...."
}

Task CompressAndProtect-ReportArchive {

	Write-Host "Checking source reports exist..."
	$serverName = $env:computername
	if ($serverName.ToUpper() -eq "LLOYD-DEV")
	{
		$reportsPath = [String]::Format("\\lloyd-dev\c$\deploy\Reports\{0}", $reportsFolderName)
	}else{
		$reportsPath = [String]::Format("\\inf-sql-mscs\Reports\{0}", $reportsFolderName)
	}
	
	if (!$reportsFolderName) 
	{
		$psake.build_success = $false
		Assert($false) "No reportsFolderName property provided, unable to find reports under $reportsPath, please rectify and run again."
	}
	Write-Host "Using reports path: $reportsPath"
	
	$reportFile = "$reportsPath\Catfish-CustomerReport.csv"
	if (!(Test-Path $reportFile)) 
	{
		$reportFile = "$reportsPath\Tesco-Cards-CustomerReport.csv"
		if (!(Test-Path $reportFile))
		{		
			$psake.build_success = $false
			Assert($false) "Source report files missing from $reportsPath, please rectify and run again."
		}
	}
	
	$reportsRunDate = (Get-Item $reportFile).LastWriteTime
	if ($reportsRunDate.Day -ne (Get-Date).Day)
	{
		$todaysDate = (Get-Date).ToShortDateString()
		$psake.build_success = $false
		Assert($false) "Source report files have not been generated in $reportsPath for $todaysDate.  Please rectify and run again."
	}
	Assert($psake.build_success -ne $true) "CompressAndProtect-ReportArchive task failed"
	
	Write-Host "Source report files exist.  Zipping and encrypting source report files."
	if (!$script:archiveFileName) 
	{
		$script:archiveFile = [String]::Format("{0}\CATFISH-DAILY-REPORTS.7z", $baseDir)
	}
	else
	{
		$script:archiveFile = [String]::Format("{0}\{1}.7z", $baseDir, $script:archiveFileName)
	}
	Write-Host "Using archive file: $script:archiveFile"
	if (test-path $script:archiveFile) { Remove-Item -force $script:archiveFile -ErrorAction SilentlyContinue }	
	
	#7z options: add, type 7z, Archive filename, Files to add (with wildcard. Change \* to \prefix* or \*.txt to limit files), compression level 9, password, encrypt filenames, Send output to host so it can be logged.
	#See http://dotnetperls.com/7-zip-examples or the 7zip help file for more info on command line switches. 
	#NOTE: -mx7 and -mx9 get around 10 to 1 compression fortext files and take round 5 minutes for 1GB of data. Use -mx3 or -mx5 to increase speed but decrease compression level.
	#Zip and Encrypt files
	exec {& $7ZipPath "a" "-t7z" "$script:archiveFile" "$reportsPath\*.csv" "-mx9" "-r" "-p$encryptionKey" "-mhe" | out-host} #Call 7z.exe to create archive file
	
	Assert($psake.build_success -ne $true) "CompressAndProtect-ReportArchive task failed"
}

Task Publish-CompressedAndProtectedReportArchive -depends CompressAndProtect-ReportArchive {

	$ftpHost = $ftpUri.Split(":")[0]
	$ftpPort = $ftpUri.Split(":")[1]
	Write-Host "Using ftpHost: $ftpHost"
	Write-Host "Using ftpPort: $ftpPort"
	
	if ($ftpPort -eq "22")
	{
		#Use psftp.exe to upload over SFTP
		$cmd = @(
		"put $script:archiveFile",
		"bye"
		)
		Write-Host "Uploading $script:archiveFile to $ftpHost"
		$cmd | & $psFtpPath -l $sftpUser -P $ftpPort -pw $sftpPassword $ftpHost
	}
	elseif ($ftpPort -eq "21")
	{
		#No need to use psftp.exe as we're operating over standard (non-secured) FTP
		$uploadFile = Get-Item $script:archiveFile
		$ftp = "ftp://$($sftpUser):$($sftpPassword)@$($ftpHost)/$($uploadFile.Name)"
		"ftp url: $ftp"

		$webClient = New-Object System.Net.WebClient
		$uri = New-Object System.Uri($ftp)

		Write-Host "Uploading $($script:archiveFile) to $ftpHost"

		$webclient.UploadFile($uri, $script:archiveFile)	
	}
}

#*================================================================================================
#* Purpose: Enumerates a list of predefined servers and retrieves disk usage and space statistics
#*================================================================================================
Task Get-ClientZoneServersDiskUsageStatistics {
	$computers = @("inf-vm-mon-02", "inf-vm-web-01", "inf-vm-web-02", "inf-vm-web-03", "inf-vm-web-04", "inf-vm-web-05", "inf-vm-app-01", "inf-vm-arr-01", "inf-vm-arr-02", "inf-vm-dc-03", "inf-vm-dc-04")
	#$computers = @("lloyd-dev")
	Get-WmiObject win32_logicaldisk -computer $computers | Where-Object { $_.DriveType -eq 3 } | Select-Object SystemName,DeviceID,VolumeName,@{Name="Size"; Expression={$_.Size/1GB}} ,@{Name="Freespace"; Expression={$_.Freespace/1GB}} 
	#Write-Host "##teamcity[buildStatisticValue key='<valueTypeKey>' value='<value>']"
}

#*================================================================================================
#* Purpose: Enumerates a list of predefined servers and retrieves disk usage and space statistics
#*================================================================================================
Task Get-DMZServersDiskUsageStatistics {
	$computers = @("inf-vm-mon-01", "inf-fs-01", "inf-vm-dc-01", "inf-vm-dc-02", "inf-vm-sql-01", "inf-vm-sql-02")
	Get-WmiObject win32_logicaldisk -computer $computers | Where-Object { $_.DriveType -eq 3 } | Select-Object SystemName,DeviceID,VolumeName,@{Name="Size"; Expression={$_.Size/1GB}} ,@{Name="Freespace"; Expression={$_.Freespace/1GB}} 
}


#*================================================================================================
#* Purpose: Enumerates a list of predefined servers and retrieves physical disk Performance counters
#*================================================================================================
Task Get-ClientZoneServersDiskPerformanceCounters {
	#$computers = @("inf-vm-mon-02", "inf-vm-web-01", "inf-vm-web-02", "inf-vm-web-03", "inf-vm-web-04", "inf-vm-web-05", "inf-vm-app-01", "inf-vm-arr-01", "inf-vm-arr-02", "inf-vm-dc-03", "inf-vm-dc-04")

	foreach ($computer in $computers)
	{
		Get-Counter -ComputerName $computer -Counter "\physicaldisk(_total)\current disk queue length"
	}
}


#*================================================================================================
#* Purpose: Downloads the MusicSales product feed from a specific location
#*================================================================================================
Task Get-Feed {

	

	#See if the directory exists
	$directory = [String]::Format("{0}\Musicsales",$baseDir)
	$DirectoryExists = (Test-Path  $directory)
	
	if(!$DirectoryExists)
	{
		md -Path $directory
	}
	
	Write-Host "Running product feed. Search server: $elasticsearchUrl --- Feed URL: $source"
	
	
	try {
		
		$verswc = $web.DownloadFile($versionSource, $versionDestination)
		$xmlVersion = [xml](get-content $versionDestination)

		$destination =  [String]::Format("{0}\Musicsales\productxml-{1}.xml",$baseDir , $xmlVersion.Products.timestamp)
		
		$FileExists = (Test-Path  $destination)
		
		if(!$FileExists)
		{
			$wc = $web.DownloadFile($source, $destination)	

			$web.Headers["Content-type"] = "application/json"
			
			$xml = [xml](get-content $destination)
			
			$jsonTop = @{
							name="ProductFeed"
						}
			$PowerShellRepresentationTop = $jsonTop | ConvertTo-Json
			
			$web.UploadString([String]::Format("{0}/productfeed/product/1",$elasticsearchUrl),"PUT",$PowerShellRepresentationTop)
			
			foreach($product in $xml.Products.Product)
			{
			
				$searchValue = $web.DownloadString([String]::Format("{0}/productfeed/product/_search?q=ProductId:{1}",$elasticsearchUrl,$product.product_id))
				
				$searchValues = $searchValue | ConvertFrom-Json
				
				$exists = $searchValues.hits[0].total
				
				
			
				#See if we have the active flag already set.
				
				$activeFlag = "0"
				
				if($exists -eq "1")
				{
					$activeFlagCheckString = $web.DownloadString([String]::Format("{0}/productfeed/product/{1}?fields=Active",$elasticsearchUrl,$product.product_id))
					
					$activeValue = $activeFlagCheckString  | ConvertFrom-Json
					
					
					if($activeValue.exists)
					{
						$activeFlag =$activeValue.fields[0].Active
					}
					else
					{
						$activeFlag = "0"
					}
				}
				
				$json = @{
							ProductId = $product.product_id;
							Title=$product.title;
							CatalogueReference=$product.catalogue_reference;
							Isbn=$product.isbn;
							Store=$product.store;
							Media=$product.media;
							Format=$product.format;
							Publisher=$product.publisher;
							PublisherGroup=$product.publisher_group;
							Series=$product.series;
							Difficulty=$product.difficulty;
							NumberOfPages=$product.number_of_pages;
							ReleaseDate=$product.release_date;
							DateAdded=$product.date_added;
							DateEdited=$product.date_edited;
							Arrangement=$product.arrangement;
							Contributors=$product.contributors;
							Genres=$product.genres;
							Language=$product.language;
							ShortDescription=$product.short_description;
							FullDescription=$product.full_description;
							Url=$product.url;
							ImageUrl=$product.image_url;
							available=$product.Available;
							Active=$activeFlag; 
						}
						
				try
				{						
					$PowerShellRepresentation = $json | ConvertTo-Json
					if($product.Available -eq "1")
					{
						$web.UploadString([String]::Format("{0}/productfeed/product/{1}",$elasticsearchUrl,$product.product_id), "PUT", $PowerShellRepresentation)
					}
					else
					{
						#remove it from the store
						$web.UploadString([String]::Format("{0}/productfeed/product/{1}",$elasticsearchUrl,$product.product_id), "DELETE", "")
						
						Write-Host [String]::Format("Deleted product {0}",$product.product_id) 
					}
				}
				catch [system.exception]
				{
					Write-host "Error while updating $product.product_id: $_.Exception.Message" -fore RED
				}
			}
			
		}
		else
		{
			Write-Host "No Change in Feed, File Exists" -fore GREEN;
		}
	}
	catch [system.exception]
	{
		Write-host "General Error: $_.Exception.Message" -fore RED
		EXIT 1
	}
}

