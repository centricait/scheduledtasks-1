#* FileName: Build-ScheduledTask.ps1
#*==========================================================================================
#* Script Name: build
#* Created: 04/12/2012
#* Author: Lloyd Holman
#* Company: Infigo Software Ltd

#* Requirements:
#* 1. Install PowerShell 2.0+ on local machine
#* 2. Execute from build.bat

#* Parameters: -task* (The build task type to run).
#*	(*) denotes required parameter, all others are optional.

#* Example use to run the default compile task:  
#* .\Build-ScheduledTasks.ps1 -task "compile"

#*==========================================================================================
#* Purpose: Wraps the core Start-ScheduledTaskDefault.ps1 script and does the following
#* - starts by importing the psake PowerShell module (we have this in a relative path in source control) .
#* - it then invokes the default psake build script in the current working folder (i.e. Start-ScheduledTaskDefault.ps1),
#* passing the first parameter passed to the batch file in as the psake task.  Start-ScheduledTaskDefault.ps1 obviously does
#* all the build work for us.
#* - finally the psake PowerShell module is removed.

#*==========================================================================================
#*==========================================================================================
#* SCRIPT BODY
#*==========================================================================================
param([string]$task = "Help", [string]$encryptionKey = "", [string]$sftpUser = "", [string]$sftpPassword = "", [string]$reportsFolderName = "", [string]$archiveFileName = "", [string]$ftpUri = "")
Import-Module '.\lib\psake\psake.psm1'; 
#$psake
$psake.use_exit_on_error = $true
Invoke-psake .\Start-ScheduledTaskDefault.ps1 -t $task -framework '4.0' -parameters @{"p1"=$encryptionKey;"p2"=$sftpUser;"p3"=$sftpPassword;"p4"=$reportsFolderName;"p5"=$archiveFileName;"p6"=$ftpUri} 
#$psake

if ($Error -ne '') 
{ 
	Write-Host "$error"
    exit $error.Count
} 
Remove-Module [p]sake -ErrorAction 'SilentlyContinue'
